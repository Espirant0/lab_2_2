#pragma once
#include <iostream>
#include <chrono>
#define N 10000000

namespace saw {
    void QuickSort(int mas[N], int a, int b);
    void BubbleSort(int mas[N], int n);
    class Timer {
    private:
        using clock_t = std::chrono::high_resolution_clock;
        using second_t = std::chrono::duration<double, std::ratio<1> >;
        std::chrono::time_point<clock_t> m_beg;
    public:
        Timer();
        void reset();
        double elapsed() const;
    };

}