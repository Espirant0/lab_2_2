#include <iostream>
#include <chrono>
#include "functions.hpp"


namespace saw {
    void QuickSort(int mas[N], int a, int b)
    {
        if (a >= b)
            return;
        int m = rand() % (b - a + 1) + a;
        int k = mas[m];
        int l = a - 1;
        int r = b + 1;
        while (1)
        {
            do l = l + 1; while (mas[l] < k);
            do r = r - 1; while (mas[r] > k);
            if (l >= r)
                break;
            std::swap(mas[l], mas[r]);
        }
        r = l;
        l = l - 1;
        QuickSort(mas, a, l);
        QuickSort(mas, r, b);
    }

    void BubbleSort(int mas[N], int n)
    {
        for (int i = 1; i < n; i++)
        {
            if (mas[i] >= mas[i - 1])
                continue;
            int j = i - 1;
            while (j >= 0 && mas[j] > mas[j + 1])
            {
                std::swap(mas[j], mas[j + 1]);
                j--;
            }
        }
    }

    Timer::Timer() : m_beg(clock_t::now()) {}
    void Timer::reset() {
        m_beg = clock_t::now();
    }
    double Timer::elapsed() const {
        return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
    }

}