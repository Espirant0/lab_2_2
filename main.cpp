#include <iostream>
#pragma comment(linker, "/STACK:400000000")
#include <chrono>
#include "functions.hpp"
#define N 1000000

using namespace saw;

int main() {
    srand(time(0));
    int mas[N];
    for (int i = 0; i < N; i++)
        mas[i] = rand();
    int n = N;
    int a = 0;
    int b = N - 1;
    Timer timer_quicksort;
    QuickSort(mas,a,b);
    std::cout << "Quicksort time: " << timer_quicksort.elapsed() << '\n';
    //Timer timer_bubblesort;
    //BubbleSort(mas, n);
    //std::cout << "Bubblesort time: " << timer_bubblesort.elapsed() << '\n';
    
   // for (int i = 0; i < N; i++)
      //  std::cout << mas[i] << std::endl;
}